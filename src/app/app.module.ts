import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SelfTourPage } from '../pages/self-tour/self-tour';
import { ListPage }  from '../pages/list/list';
import { AmbrypagePage }        from '../pages/ambrypage/ambrypage';
import { AmboPage } from '../pages/ambo/ambo';
import { AlterPage } from '../pages/alter/alter';
import { BaptismalfontPage }      from '../pages/baptismalfont/baptismalfont';
import { CathedraPage } from '../pages/cathedra/cathedra';
import { EucharisticchapelPage } from '../pages/eucharisticchapel/eucharisticchapel';
import { StainedglasspagePage }        from '../pages/stainedglasspage/stainedglasspage';
import { OrganpipesPage } from '../pages/organpipes/organpipes';
import { ShrinetomaryPage } from '../pages/shrinetomary/shrinetomary';
import { EastercandlePage } from '../pages/eastercandle/eastercandle';
import { InsidethecathedralPage } from '../pages/insidethecathedral/insidethecathedral';
import { SanctuaryPage} from '../pages/sanctuary/sanctuary';
import { ReliefPage} from '../pages/relief/relief';
import { ReconciliationchapelPage } from '../pages/reconciliationchapel/reconciliationchapel';
import { DiagramPage } from '../pages/diagram/diagram';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Keyboard } from '@ionic-native/keyboard';
import { SmartAudioProvider } from '../providers/smart-audio/smart-audio';
import { NativeAudio } from '@ionic-native/native-audio';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    SelfTourPage,
    AmbrypagePage,
    AmboPage,
    AlterPage,
    BaptismalfontPage,
    CathedraPage,
    EucharisticchapelPage,
    OrganpipesPage,
    StainedglasspagePage,
    ShrinetomaryPage,
    EastercandlePage,
    InsidethecathedralPage,
    SanctuaryPage,
    ReliefPage,
    ReconciliationchapelPage,
    DiagramPage,
    ListPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    SelfTourPage,
    AmbrypagePage,
    AmboPage,
    AlterPage,
    BaptismalfontPage,
    CathedraPage,
    EucharisticchapelPage,
    OrganpipesPage,
    StainedglasspagePage,
    ShrinetomaryPage,
    EastercandlePage,
    InsidethecathedralPage,
    SanctuaryPage,
    ReliefPage,
    ReconciliationchapelPage,
    DiagramPage,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NativeAudio,
    SmartAudioProvider
  ]
})
export class AppModule {}
