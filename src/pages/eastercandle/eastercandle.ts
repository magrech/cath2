import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {BaptismalfontPage} from '../baptismalfont/baptismalfont';
import {InsidethecathedralPage} from '../insidethecathedral/insidethecathedral';
/**
 * Generated class for the EastercandlePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-eastercandle',
  templateUrl: 'eastercandle.html',
})
export class EastercandlePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

baptismalfontpage() {
     this.navCtrl.push(BaptismalfontPage);
 
 }
 insidethecathedralpage() {
  this.navCtrl.push(InsidethecathedralPage);
 }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad EastercandlePage');
  }

}
