import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EastercandlePage } from './eastercandle';

@NgModule({
  declarations: [
    EastercandlePage,
  ],
  imports: [
    IonicPageModule.forChild(EastercandlePage),
  ],
  exports: [
    EastercandlePage
  ]
})
export class EastercandlePageModule {}
