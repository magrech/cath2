import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganpipesPage } from './organpipes';

@NgModule({
  declarations: [
    OrganpipesPage,
  ],
  imports: [
    IonicPageModule.forChild(OrganpipesPage),
  ],
  exports: [
    OrganpipesPage
  ]
})
export class OrganpipesPageModule {}
