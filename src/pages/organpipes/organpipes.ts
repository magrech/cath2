import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SanctuaryPage} from '../sanctuary/sanctuary';
import {ReliefPage} from '../relief/relief';
/**
 * Generated class for the OrganpipesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-organpipes',
  templateUrl: 'organpipes.html',
})
export class OrganpipesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
 sanctuarypage() {
  this.navCtrl.push(SanctuaryPage);
 }
 
 reliefpage() {
  this.navCtrl.push(ReliefPage);
 }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OrganpipesPage');
  }

}
