import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AmbrypagePage } from './ambrypage';

@NgModule({
  declarations: [
    AmbrypagePage,
  ],
  imports: [
    IonicPageModule.forChild(AmbrypagePage),
  ],
  exports: [
    AmbrypagePage
  ]
})
export class AmbrypagePageModule {}
