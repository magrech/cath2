import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CathedraPage} from '../cathedra/cathedra';
import {EucharisticchapelPage} from '../eucharisticchapel/eucharisticchapel';
/**
 * Generated class for the AmbrypagePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-ambrypage',
  templateUrl: 'ambrypage.html'
})
export class AmbrypagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

cathedrapage() {
     this.navCtrl.push(CathedraPage);
 }
 eucharisticchapelpage() {
      this.navCtrl.push(EucharisticchapelPage);
 }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad AmbrypagePage');
  }

}
