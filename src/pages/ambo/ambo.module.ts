import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AmboPage } from './ambo';

@NgModule({
  declarations: [
    AmboPage,
  ],
  imports: [
    IonicPageModule.forChild(AmboPage),
  ],
  exports: [
    AmboPage
  ]
})
export class AmboPageModule {}
