import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ReconciliationchapelPage} from '../reconciliationchapel/reconciliationchapel';
import {AlterPage} from '../alter/alter';

/**
 * Generated class for the AmboPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-ambo',
  templateUrl: 'ambo.html',
})
export class AmboPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


 reconciliationchapelpage() {
  this.navCtrl.push(ReconciliationchapelPage)
 }
 
 alterpage() {
     this.navCtrl.push(AlterPage);
 
 }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad AmboPage');
  }

}
