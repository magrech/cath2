import { Component } from '@angular/core';

import { SelfTourPage } from '../self-tour/self-tour';
import { ListPage }  from '../list/list';
//import { AboutPage } from '../about/about';
//import { ContactPage } from '../contact/contact'; 
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = SelfTourPage;
  tab3Root = ListPage;

  constructor() {

  }
}
