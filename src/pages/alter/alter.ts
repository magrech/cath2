import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AmboPage} from '../ambo/ambo';
import {CathedraPage} from '../cathedra/cathedra';
/**
 * Generated class for the AlterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-alter',
  templateUrl: 'alter.html',
})
export class AlterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

 ambopage() {
     this.navCtrl.push(AmboPage);
    // this.navCtrl.push(ambrypage);
 }
 cathedrapage() {
     this.navCtrl.push(CathedraPage);
 }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad AlterPage');
  }

}
