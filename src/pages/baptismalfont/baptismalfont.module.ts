import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BaptismalfontPage } from './baptismalfont';

@NgModule({
  declarations: [
    BaptismalfontPage,
  ],
  imports: [
    IonicPageModule.forChild(BaptismalfontPage),
  ],
  exports: [
    BaptismalfontPage
  ]
})
export class BaptismalfontPageModule {}
