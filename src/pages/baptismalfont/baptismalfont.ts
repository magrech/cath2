import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ShrinetomaryPage} from '../shrinetomary/shrinetomary';
import {EastercandlePage} from '../eastercandle/eastercandle';
/**
 * Generated class for the BaptismalfontPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-baptismalfont',
  templateUrl: 'baptismalfont.html',
})
export class BaptismalfontPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

 shrinetomarypage() {
  this.navCtrl.push(ShrinetomaryPage);
 }
 
 eastercandlepage() {
  this.navCtrl.push(EastercandlePage);
 }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad BaptismalfontPage');
  }

}
