import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StainedglasspagePage } from '../stainedglasspage/stainedglasspage';
import {OrganpipesPage} from '../organpipes/organpipes';
/**
 * Generated class for the SanctuaryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-sanctuary',
  templateUrl: 'sanctuary.html',
})
export class SanctuaryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

 stainedglasspage() {
    this.navCtrl.push(StainedglasspagePage);
     }
     
     
 organpipespage() {
      this.navCtrl.push(OrganpipesPage);
 }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad SanctuaryPage');
  }

}
