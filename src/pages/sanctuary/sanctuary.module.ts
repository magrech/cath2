import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SanctuaryPage } from './sanctuary';

@NgModule({
  declarations: [
    SanctuaryPage,
  ],
  imports: [
    IonicPageModule.forChild(SanctuaryPage),
  ],
  exports: [
    SanctuaryPage
  ]
})
export class SanctuaryPageModule {}
