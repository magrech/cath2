import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlterPage} from '../alter/alter';
import { AmbrypagePage }        from '../ambrypage/ambrypage';
/**
 * Generated class for the CathedraPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-cathedra',
  templateUrl: 'cathedra.html',
})
export class CathedraPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
 ambrypage() {
     this.navCtrl.push(AmbrypagePage);
    // this.navCtrl.push(ambrypage);
 }
 alterpage() {
     this.navCtrl.push(AlterPage);
 
 }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad CathedraPage');
  }

}
