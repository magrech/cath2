import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CathedraPage } from './cathedra';

@NgModule({
  declarations: [
    CathedraPage,
  ],
  imports: [
    IonicPageModule.forChild(CathedraPage),
  ],
  exports: [
    CathedraPage
  ]
})
export class CathedraPageModule {}
