import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ReliefPage} from '../relief/relief';
import {AmboPage} from '../ambo/ambo';
/**
 * Generated class for the ReconciliationchapelPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-reconciliationchapel',
  templateUrl: 'reconciliationchapel.html',
})
export class ReconciliationchapelPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

 
 reliefpage() {
  this.navCtrl.push(ReliefPage);
 }
  ambopage() {
     this.navCtrl.push(AmboPage);
    // this.navCtrl.push(ambrypage);
 }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ReconciliationchapelPage');
  }

}
