import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReconciliationchapelPage } from './reconciliationchapel';

@NgModule({
  declarations: [
    ReconciliationchapelPage,
  ],
  imports: [
    IonicPageModule.forChild(ReconciliationchapelPage),
  ],
  exports: [
    ReconciliationchapelPage
  ]
})
export class ReconciliationchapelPageModule {}
