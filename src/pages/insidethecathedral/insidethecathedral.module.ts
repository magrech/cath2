import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InsidethecathedralPage } from './insidethecathedral';

@NgModule({
  declarations: [
    InsidethecathedralPage,
  ],
  imports: [
    IonicPageModule.forChild(InsidethecathedralPage),
  ],
  exports: [
    InsidethecathedralPage
  ]
})
export class InsidethecathedralPageModule {}
