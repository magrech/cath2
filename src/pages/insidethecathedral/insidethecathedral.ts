import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {EastercandlePage} from '../eastercandle/eastercandle';
import { StainedglasspagePage } from '../stainedglasspage/stainedglasspage';
/**
 * Generated class for the InsidethecathedralPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-insidethecathedral',
  templateUrl: 'insidethecathedral.html',
})
export class InsidethecathedralPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

eastercandlepage() {
  this.navCtrl.push(EastercandlePage);
 }
 
  stainedglasspage() {
    this.navCtrl.push(StainedglasspagePage);
     }
     
  ionViewDidLoad() {
    console.log('ionViewDidLoad InsidethecathedralPage');
  }

}
