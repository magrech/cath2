import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { StainedglasspagePage } from '../stainedglasspage/stainedglasspage';
import { AmbrypagePage }        from '../ambrypage/ambrypage';
import { OrganpipesPage }  from '../organpipes/organpipes';
import {ReconciliationchapelPage } from '../reconciliationchapel/reconciliationchapel';
import {AmboPage} from '../ambo/ambo';
import {AlterPage} from '../alter/alter';
import {CathedraPage} from '../cathedra/cathedra';
import {ReliefPage} from '../relief/relief';
import {SanctuaryPage} from '../sanctuary/sanctuary';
import {EucharisticchapelPage} from '../eucharisticchapel/eucharisticchapel';
import {InsidethecathedralPage} from '../insidethecathedral/insidethecathedral';
import {EastercandlePage} from '../eastercandle/eastercandle';
import {BaptismalfontPage} from '../baptismalfont/baptismalfont';
import {ShrinetomaryPage} from '../shrinetomary/shrinetomary';


/**
 * Generated class for the SelfTourPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-self-tour',
  templateUrl: 'self-tour.html',
})
export class SelfTourPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelfTourPage');
  }
ambrypage() {
     this.navCtrl.push(AmbrypagePage);

 }
 
  stainedglasspage() {
    this.navCtrl.push(StainedglasspagePage);
     }
   
   organpipespage() {
    this.navCtrl.push(OrganpipesPage);
   }
   reconciliationchapelpage() {
    this.navCtrl.push(ReconciliationchapelPage);
   }
   ambopage() {
    this.navCtrl.push(AmboPage);
   }
   alterpage() {
    this.navCtrl.push(AlterPage);
   }
   cathedrapage() {
    this.navCtrl.push(CathedraPage);
   }
   reliefpage() {
    this.navCtrl.push(ReliefPage);
   }
   sanctuarypage() {
    this.navCtrl.push(SanctuaryPage);
   }
   eucharisticchapelpage() {
    this.navCtrl.push(EucharisticchapelPage);
   }
   insidethecathedralpage() {
    this.navCtrl.push(InsidethecathedralPage);
   }
   eastercandlepage() {
    this.navCtrl.push(EastercandlePage);
   }
   baptismalfontpage() {
    this.navCtrl.push(BaptismalfontPage);
   }
   shrinetomarypage() {
    this.navCtrl.push(ShrinetomaryPage);
   }
}
