import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {InsidethecathedralPage} from '../insidethecathedral/insidethecathedral';
import {SanctuaryPage} from '../sanctuary/sanctuary';
/**
 * Generated class for the StainedglasspagePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-stainedglasspage',
  templateUrl: 'stainedglasspage.html',
})
export class StainedglasspagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
insidethecathedralpage() {
  this.navCtrl.push(InsidethecathedralPage);
 }
 
 sanctuarypage() {
  this.navCtrl.push(SanctuaryPage);
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StainedglasspagePage');
  }

}
