import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StainedglasspagePage } from './stainedglasspage';

@NgModule({
  declarations: [
    StainedglasspagePage,
  ],
  imports: [
    IonicPageModule.forChild(StainedglasspagePage),
  ],
  exports: [
    StainedglasspagePage
  ]
})
export class StainedglasspagePageModule {}
