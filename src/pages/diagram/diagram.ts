import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { StainedglasspagePage } from '../stainedglasspage/stainedglasspage';
import { AmbrypagePage }        from '../ambrypage/ambrypage';
import {AmboPage} from '../ambo/ambo';
import {AlterPage} from '../alter/alter';
import {BaptismalfontPage} from '../baptismalfont/baptismalfont';
import {CathedraPage} from '../cathedra/cathedra';
import {EucharisticchapelPage} from '../eucharisticchapel/eucharisticchapel';
import {OrganpipesPage} from '../organpipes/organpipes';
import {ShrinetomaryPage} from '../shrinetomary/shrinetomary';
import {EastercandlePage} from '../eastercandle/eastercandle';
import {InsidethecathedralPage} from '../insidethecathedral/insidethecathedral';
import {SanctuaryPage} from '../sanctuary/sanctuary';
import {ReliefPage} from '../relief/relief';
import {ReconciliationchapelPage} from '../reconciliationchapel/reconciliationchapel';

/**
 * Generated class for the DiagramPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-diagram',
  templateUrl: 'diagram.html',
})
export class DiagramPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
shrinetomarypage() {
  this.navCtrl.push(ShrinetomaryPage);
 }
 
 eastercandlepage() {
  this.navCtrl.push(EastercandlePage);
 }
 
 insidethecathedralpage() {
  this.navCtrl.push(InsidethecathedralPage);
 }
 
 sanctuarypage() {
  this.navCtrl.push(SanctuaryPage);
 }
 
 reliefpage() {
  this.navCtrl.push(ReliefPage);
 }
 
 reconciliationchapelpage() {
  this.navCtrl.push(ReconciliationchapelPage)
 }
 ambrypage() {
     this.navCtrl.push(AmbrypagePage);
    // this.navCtrl.push(ambrypage);
 }
 
  stainedglasspage() {
    this.navCtrl.push(StainedglasspagePage);
     }
  ambopage() {
     this.navCtrl.push(AmboPage);
    // this.navCtrl.push(ambrypage);
 }
 alterpage() {
     this.navCtrl.push(AlterPage);
 
 }
 baptismalfontpage() {
     this.navCtrl.push(BaptismalfontPage);
 
 }
 cathedrapage() {
     this.navCtrl.push(CathedraPage);
 }
 eucharisticchapelpage() {
      this.navCtrl.push(EucharisticchapelPage);
 }
 organpipespage() {
      this.navCtrl.push(OrganpipesPage);
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DiagramPage');
  }

}
