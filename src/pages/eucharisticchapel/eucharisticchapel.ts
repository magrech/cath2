import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AmbrypagePage }        from '../ambrypage/ambrypage';
/**
 * Generated class for the EucharisticchapelPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-eucharisticchapel',
  templateUrl: 'eucharisticchapel.html',
})
export class EucharisticchapelPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
 ambrypage() {
     this.navCtrl.push(AmbrypagePage);
    // this.navCtrl.push(ambrypage);
 }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad EucharisticchapelPage');
  }

}
