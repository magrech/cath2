import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EucharisticchapelPage } from './eucharisticchapel';

@NgModule({
  declarations: [
    EucharisticchapelPage,
  ],
  imports: [
    IonicPageModule.forChild(EucharisticchapelPage),
  ],
  exports: [
    EucharisticchapelPage
  ]
})
export class EucharisticchapelPageModule {}
