import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {BaptismalfontPage} from '../baptismalfont/baptismalfont';
/**
 * Generated class for the ShrinetomaryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-shrinetomary',
  templateUrl: 'shrinetomary.html',
})
export class ShrinetomaryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
baptismalfontpage() {
     this.navCtrl.push(BaptismalfontPage);
 
 }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ShrinetomaryPage');
  }

}
