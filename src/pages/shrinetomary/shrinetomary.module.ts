import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShrinetomaryPage } from './shrinetomary';

@NgModule({
  declarations: [
    ShrinetomaryPage,
  ],
  imports: [
    IonicPageModule.forChild(ShrinetomaryPage),
  ],
  exports: [
    ShrinetomaryPage
  ]
})
export class ShrinetomaryPageModule {}
