import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReliefPage } from './relief';

@NgModule({
  declarations: [
    ReliefPage,
  ],
  imports: [
    IonicPageModule.forChild(ReliefPage),
  ],
  exports: [
    ReliefPage
  ]
})
export class ReliefPageModule {}
