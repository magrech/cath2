import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {OrganpipesPage} from '../organpipes/organpipes';
import {ReconciliationchapelPage} from '../reconciliationchapel/reconciliationchapel';
/**
 * Generated class for the ReliefPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-relief',
  templateUrl: 'relief.html',
})
export class ReliefPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

 reconciliationchapelpage() {
  this.navCtrl.push(ReconciliationchapelPage)
 }
 organpipespage() {
      this.navCtrl.push(OrganpipesPage);
 }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ReliefPage');
  }

}
